<?php

namespace Kareem\ControlPanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('KareemControlPanelBundle:Default:index.html.twig');
    }
}
